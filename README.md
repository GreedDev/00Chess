File: README.md<br>
Author: Michalis Vardoulakis <vardoulakism@gmail.com><br>
<br>
<dl>
<dt>First time git setup</dt>
git config --global user.name "yourname"<br>
git config --global user.email "yourmail"</dt>
<dt>Set up the repository (command line)</dt>
<dd>git clone https://gitlab.com/GreedDev/00Chess.git<br>
This creates a new folder '00Chess' containing all out files.<br>
Inside the folder there will be a folder called '.git' which you should
never touch unless you know what you are doing.</dd>
</dl>
<br>
For this project, we'll be using NetBeans IDE. To open the project in NetBeans
you need to go to Team > Git > Clone, select HTTPS for the Repository URL and
copy-paste https://gitlab.com/GreedDev/00Chess.git in that field.
Insert your gitlab user information in the boxes below. Then click next. Leave 
master as the selected branch, click next again. Then you only need to select
the project folder and you are set.