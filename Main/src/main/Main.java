/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import Model.Board.*;
import Model.Pieces.*;
import java.util.ArrayList;

/**
 *
 * @author darthinvader
 */
public class Main {

    /** 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Board board  = new Board();
        board.PrintBoardState();
    }
    
}
