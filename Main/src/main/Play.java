/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import Model.Board.Board;
import Model.Board.BoardPoint;
import Model.Pieces.*;
import java.util.ArrayList;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.state.*;
import org.newdawn.slick.*;

/**
 *
 * @author DarthInvader
 */
public class Play extends BasicGameState {

    boolean downFlag;
    int tileSize = 50;
    Board board;
    Image BlackSquare;
    Image WhiteSquare;
    Image Background;
    Image GreenSquare;
    Image Blacks[];
    Image Whites[];
    ArrayList<BoardPoint> moveables;
    int xTile;
    int yTile;
    boolean playerTurn;
    BoardPoint SelectedPiece;
    String mousepos;
    boolean pawnPromotion;
    BoardPoint Movement;
    public Play(int state) {
    }

    @Override
    public int getID() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return 1;
    }

    @Override
    public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
        xTile = -1;
        yTile = -1;
        board = new Board();
        BlackSquare = new Image("res/Sprites/BrownSquare.png");
        WhiteSquare = new Image("res/Sprites/WhiteSquare.png");
        Background = new Image("res/Sprites/chessbackground.png");
        Blacks = new Image[6];
        Whites = new Image[6];
        Blacks[0] = new Image("res/Sprites/blackPawn.png");
        Blacks[1] = new Image("res/Sprites/blackRook.png");
        Blacks[2] = new Image("res/Sprites/blackKnight.png");
        Blacks[3] = new Image("res/Sprites/blackBishop.png");
        Blacks[4] = new Image("res/Sprites/blackQueen.png");
        Blacks[5] = new Image("res/Sprites/blackKing.png");
        Whites[0] = new Image("res/Sprites/whitePawn.png");
        Whites[1] = new Image("res/Sprites/whiteRook.png");
        Whites[2] = new Image("res/Sprites/whiteKnight.png");
        Whites[3] = new Image("res/Sprites/whiteBishop.png");
        Whites[4] = new Image("res/Sprites/whiteQueen.png");
        Whites[5] = new Image("res/Sprites/whiteKing.png");
        GreenSquare = new Image("res/Sprites/greenTransparentSquare.png");
        playerTurn = board.getPlayerTurn();
        moveables = null;
        SelectedPiece = null;
        mousepos = "";
        downFlag = false;
        pawnPromotion = false;
        Movement = null;
    }

    @Override
    public void render(GameContainer gc, StateBasedGame sbg, Graphics grphcs) throws SlickException {
        grphcs.drawString("you made it", 50, 50);
        gc.getHeight();
        gc.getWidth();
        Image chessPiece;

        Background.draw(0, 0, 1280, 720);
        grphcs.drawString(mousepos, 50, 50);
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if ((i + j) % 2 == 0) {
                    BlackSquare.draw(440 + i * 50, 160 + j * 50);
                } else {
                    WhiteSquare.draw(440 + i * 50, 160 + j * 50);
                }
            }
        }
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                chessPiece = FindChessPiece(i, j);
                if (chessPiece != null) {
                    grphcs.drawImage(chessPiece, 440 + i * 50, 160 + j * 50);
                }
            }
        }
        PrintMoves();
        PrintPawnPromotion();
    }

    @Override
    public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
        playerTurn = board.getPlayerTurn();
        int x = Mouse.getX();
        int y = gc.getHeight() - Mouse.getY();
        xTile = (x - 440) / 50;
        yTile = (y - 160) / 50;
        BoardPoint pos = new BoardPoint((x - 440) / 50, (y - 160) / 50);
        mousepos = "x:" + x + "y:" + y + "xTile" + xTile + "yTile" + yTile;
        if (Mouse.isButtonDown(0)) {
            downFlag = true;
        }
        if (!Mouse.isButtonDown(0) && downFlag) {
            downFlag = false;
            if (!pawnPromotion) {
                SelectOrMovePiece(pos);
            } else {
                SelectPromotion(x, y);
            }
        }
        if(SelectedPiece!=null){
            System.out.println(SelectedPiece.getX());
        }

    }

    private Image FindChessPiece(int x, int y) {
        Piece curr = board.getPiece(new BoardPoint(x, y));
        if (curr == null) {
            return null;
        }
        if (curr.getPieceColor() == true) {
            if (curr instanceof Pawn) {
                return Whites[0];
            }
            if (curr instanceof Rook) {
                return Whites[1];
            }
            if (curr instanceof Knight) {
                return Whites[2];
            }
            if (curr instanceof Bishop) {
                return Whites[3];
            }
            if (curr instanceof Queen) {
                return Whites[4];
            }
            if (curr instanceof King) {
                return Whites[5];
            }
        } else {
            if (curr instanceof Pawn) {
                return Blacks[0];
            }
            if (curr instanceof Rook) {
                return Blacks[1];
            }
            if (curr instanceof Knight) {
                return Blacks[2];
            }
            if (curr instanceof Bishop) {
                return Blacks[3];
            }
            if (curr instanceof Queen) {
                return Blacks[4];
            }
            if (curr instanceof King) {
                return Blacks[5];
            }
        }
        return null;
    }

    private void SelectOrMovePiece(BoardPoint pos) {
        if (SelectedPiece != null) {
            if (moveables != null) {
                for (BoardPoint p : moveables) {
                    if (p.equals(pos)) {
                        int flag = board.MovePiece(SelectedPiece, pos);
                        Movement = pos;
                        if (flag == 1) {
                            pawnPromotion = true;
                        }
                        System.out.println(flag);
                        moveables = null;
                        return;
                    }
                }
            } else {
                SelectedPiece = null;
            }
        }
        Piece p = board.getPiece(pos);
        if (p != null && p.getPieceColor() == playerTurn) {
            SelectedPiece = pos;
            moveables = board.getPiecePossibleMoves(pos);
        }
    }

    private void PrintMoves() {
        if (moveables != null) {
            moveables.forEach((p) -> {
                GreenSquare.draw(440 + p.getX() * 50, 160 + p.getY() * 50);
            });
        }
    }

    private void PrintPawnPromotion() {
        if (pawnPromotion) {
            if (playerTurn) {
                Blacks[1].draw(800, 600);
                Blacks[2].draw(850, 600);
                Blacks[3].draw(900, 600);
                Blacks[4].draw(950, 600);
            } else {
                Whites[1].draw(800, 600);
                Whites[2].draw(850, 600);
                Whites[3].draw(900, 600);
                Whites[4].draw(950, 600);
            }
        }
    }

    private void SelectPromotion(int x, int y) {
        if (y >= 600 && y < 650) {
            boolean flag = false;
            if (x >= 800 && x < 850) {
                board.promotePawn(1, Movement);
                flag = true;
            }
            if (x >= 850 && x < 900) {
                board.promotePawn(2, Movement);
                flag = true;
            }
            if (x >= 900 && x < 950) {
                board.promotePawn(3, Movement);
                flag = true;
            }
            if (x >= 950 && x < 1000) {
                board.promotePawn(4, Movement);
                flag = true;
            }
            if(flag){
                pawnPromotion = false;
            }
        }
    }

}
