/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import Model.Board.Board;
import org.newdawn.slick.state.*;
import org.newdawn.slick.*;
/**
 *
 * @author DarthInvader
 */
public class Viewer extends StateBasedGame{
    public static final String gamename = "01Chess";
    public static final int menu = 0;
    public static final int play = 1;
    public Viewer(String gamename){
        super(gamename);
        this.addState(new Menu(menu));
        this.addState(new Play(play));
    }
    public static void main(String[] args){
        Board board  = new Board();
        board.PrintBoardState();
        AppGameContainer appgc;
        try{
            appgc = new AppGameContainer(new Viewer(gamename));
            appgc.setDisplayMode(1280, 720, false);
            appgc.setAlwaysRender(true);
            appgc.setShowFPS(true);
            appgc.setMusicVolume(1.f);
            appgc.start();
        }catch(SlickException e){
            e.printStackTrace();
        }
    }

    @Override
    public void initStatesList(GameContainer gc) throws SlickException {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        this.getState(menu).init(gc, this);
        this.getState(play).init(gc, this);
        this.enterState(menu);
    }
}
