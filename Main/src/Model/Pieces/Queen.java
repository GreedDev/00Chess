package Model.Pieces;

import Model.Board.BoardPoint;
import java.util.ArrayList;

/**
 *
 * @author mvard
 * @author darthinvader
 */
public class Queen extends Piece {

    public Queen(boolean isWhite, BoardPoint position) {
        super(isWhite, position);
    }

    @Override
    public ArrayList<BoardPoint> getPossibleMoves(BoardPoint dimensions, Piece[][] board) {
        ArrayList<BoardPoint> PossibleMoves = new ArrayList<>();
        Piece Rook = new Rook(this.getPieceColor(), this.getPosition());
        ArrayList<BoardPoint> moves = Rook.getPossibleMoves(dimensions, board);
        if (!moves.isEmpty()) {
            PossibleMoves.addAll(moves);
        }
        Piece Bishop = new Bishop(this.getPieceColor(), this.getPosition());
        moves = Bishop.getPossibleMoves(dimensions, board);
        if (!moves.isEmpty()) {
            PossibleMoves.addAll(moves);
        }
        return PossibleMoves;
    }
    
    @Override
    public ArrayList<BoardPoint> getThreatArea(BoardPoint dimensions, Piece[][] board) {
        ArrayList<BoardPoint> threatArea = new ArrayList<>();
        Piece Rook = new Rook(this.getPieceColor(), this.getPosition());
        ArrayList<BoardPoint> moves = Rook.getThreatArea(dimensions, board);
        if (!moves.isEmpty()) {
            threatArea.addAll(moves);
        }
        Piece Bishop = new Bishop(this.getPieceColor(), this.getPosition());
        moves = Bishop.getThreatArea(dimensions, board);
        if (!moves.isEmpty()) {
            threatArea.addAll(moves);
        }
        return threatArea;
    }
    @Override
    public Piece GetCopy() {
        return (new Queen(isWhite,position));
    }
}
