package Model.Pieces;

import Model.Board.BoardPoint;
import java.util.ArrayList;

/**
 *
 * @author mvard
 * @author darthinvader
 */
public class Bishop extends Piece {

    public Bishop(boolean isWhite, BoardPoint position) {
        super(isWhite, position);
    }

    /**
     *
     * @param dimensions
     * @param board
     * @return
     */
    @Override
    public ArrayList<BoardPoint> getPossibleMoves(BoardPoint dimensions, Piece[][] board) {
        ArrayList<BoardPoint> PossibleMoves = new ArrayList<>();
        ArrayList<BoardPoint> buffer = getUpperRightDiag(dimensions, board,false);
        if (!buffer.isEmpty()) {
            PossibleMoves.addAll(buffer);
        }
        buffer = getUpperLeftDiag(dimensions, board,false);
        if (!buffer.isEmpty()) {
            PossibleMoves.addAll(buffer);
        }
        buffer = getDownRightDiag(dimensions, board,false);
        if (!buffer.isEmpty()) {
            PossibleMoves.addAll(buffer);
        }
        buffer = getDownLeftDiag(dimensions, board,false);
        if (!buffer.isEmpty()) {
            PossibleMoves.addAll(buffer);
        }
        return PossibleMoves;
    }

    private ArrayList<BoardPoint> getUpperRightDiag(BoardPoint dimensions, Piece[][] board,boolean Threat) {
        ArrayList<BoardPoint> UpperRightDiag = new ArrayList<>();
        int x = position.getX();
        int y = position.getY();
        while (true) {
            x++;
            y++;
            if (x >= dimensions.getX() || y >= dimensions.getY()) {
                break;
            }
            if (board[x][y] != null) {
                if (board[x][y].ComparePieceColors(this)) {
                    if(Threat){
                        UpperRightDiag.add(new BoardPoint(x,y));
                    }
                    break;
                } else if (!(board[x][y] instanceof King) || !Threat) {
                    UpperRightDiag.add(new BoardPoint(x, y));
                    break;
                }
            }
            UpperRightDiag.add(new BoardPoint(x, y));
        }
        return UpperRightDiag;
    }

    private ArrayList<BoardPoint> getUpperLeftDiag(BoardPoint dimensions, Piece[][] board,boolean Threat) {
        ArrayList<BoardPoint> UpperRightDiag = new ArrayList<>();
        int x = position.getX();
        int y = position.getY();
        while (true) {
            x++;
            y--;
            if (x >= dimensions.getX() || y < 0) {
                break;
            }
            if (board[x][y] != null) {
                if (board[x][y].ComparePieceColors(this)) {
                    if(Threat){
                        UpperRightDiag.add(new BoardPoint(x,y));
                    }
                    break;
                } else if (!(board[x][y] instanceof King) || !Threat) {
                    UpperRightDiag.add(new BoardPoint(x, y));
                    break;
                }
            }
            UpperRightDiag.add(new BoardPoint(x, y));
        }
        return UpperRightDiag;
    }

    private ArrayList<BoardPoint> getDownRightDiag(BoardPoint dimensions, Piece[][] board,boolean Threat) {
        ArrayList<BoardPoint> UpperRightDiag = new ArrayList<>();
        int x = position.getX();
        int y = position.getY();
        while (true) {
            x--;
            y++;
            if (x < 0 || y >= dimensions.getY()) {
                break;
            }
            if (board[x][y] != null) {
                if (board[x][y].ComparePieceColors(this)) {
                    if(Threat){
                        UpperRightDiag.add(new BoardPoint(x,y));
                    }
                    break;
                } else if (!(board[x][y] instanceof King) || !Threat) {
                    UpperRightDiag.add(new BoardPoint(x, y));
                    break;
                }
            }
            UpperRightDiag.add(new BoardPoint(x, y));
        }
        return UpperRightDiag;
    }

    private ArrayList<BoardPoint> getDownLeftDiag(BoardPoint dimensions, Piece[][] board,boolean Threat) {
        ArrayList<BoardPoint> UpperRightDiag = new ArrayList<>();
        int x = position.getX();
        int y = position.getY();
        while (true) {
            x--;
            y--;
            if (x < 0 || y < 0) {
                break;
            }
            if (board[x][y] != null) {
                if (board[x][y].ComparePieceColors(this)) {
                    if(Threat){
                        UpperRightDiag.add(new BoardPoint(x,y));
                    }
                    break;
                } else if (!(board[x][y] instanceof King) || !Threat) {
                    UpperRightDiag.add(new BoardPoint(x, y));
                    break;
                }
            }
            UpperRightDiag.add(new BoardPoint(x, y));
        }
        return UpperRightDiag;
    }

    @Override
    public ArrayList<BoardPoint> getThreatArea(BoardPoint dimensions, Piece[][] board) {
        ArrayList<BoardPoint> PossibleMoves = new ArrayList<>();
        ArrayList<BoardPoint> buffer = getUpperRightDiag(dimensions, board,true);
        if (!buffer.isEmpty()) {
            PossibleMoves.addAll(buffer);
        }
        buffer = getUpperLeftDiag(dimensions, board,true);
        if (!buffer.isEmpty()) {
            PossibleMoves.addAll(buffer);
        }
        buffer = getDownRightDiag(dimensions, board,true);
        if (!buffer.isEmpty()) {
            PossibleMoves.addAll(buffer);
        }
        buffer = getDownLeftDiag(dimensions, board,true);
        if (!buffer.isEmpty()) {
            PossibleMoves.addAll(buffer);
        }
        return PossibleMoves;
    
    }

    @Override
    public Piece GetCopy() {
        return (new Bishop(isWhite,position));
    }
}
