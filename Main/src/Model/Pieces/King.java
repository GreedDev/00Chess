/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Pieces;

import Model.Board.BoardPoint;
import java.util.ArrayList;

/**
 *
 * @author mvard
 * @author darthinvader
 */
public class King extends Piece {

    public King(boolean isWhite, BoardPoint position) {
        super(isWhite, position);
    }

    @Override
    public ArrayList<BoardPoint> getPossibleMoves(BoardPoint dimensions, Piece[][] board) {
        ArrayList<BoardPoint> PossibleMoves = KingPossibles(dimensions,board);
        ArrayList<BoardPoint> EnemyMoves = KingNonAccessible(dimensions, board);
        if(!EnemyMoves.isEmpty()){
            EnemyMoves.forEach((p) -> {
                System.out.println(p.getX() + " : " + p.getY());
            });
        }
        
        for(int i=0;i<PossibleMoves.size();i++){
            for(BoardPoint e:EnemyMoves){
                if(PossibleMoves.get(i).equals(e)){
                    PossibleMoves.remove(i);
                    i--;
                    break;
                }
            }
        }
        return PossibleMoves;
    }

    public ArrayList<BoardPoint> KingPossibles(BoardPoint dimensions, Piece[][] board) {
        ArrayList<BoardPoint> possibleMoves = new ArrayList<>();
        int x = dimensions.getX();
        int y = dimensions.getY();
        int X = position.getX();
        int Y = position.getY();
        if (X - 1 >= 0) {
            if ((board[X - 1][Y] == null) || (board[X - 1][Y] != null && !ComparePieceColors(board[X - 1][Y]))) {
                possibleMoves.add(new BoardPoint(X - 1, Y));
            }
            
            if (Y - 1 >= 0 && ((board[X - 1][Y - 1] == null) || (board[X - 1][Y - 1] != null && !ComparePieceColors(board[X - 1][Y - 1])))) {
                possibleMoves.add(new BoardPoint(X - 1, Y - 1));
            }
            if (Y + 1 < y && ((board[X - 1][Y + 1] == null) || (board[X - 1][Y + 1] != null && !ComparePieceColors(board[X - 1][Y + 1])))) {
                possibleMoves.add(new BoardPoint(X - 1, Y + 1));
            }
        }
        if (X + 1 < x) {
            if ((board[X + 1][Y] == null) || (board[X + 1][Y] != null && !ComparePieceColors(board[X + 1][Y]))) {
                possibleMoves.add(new BoardPoint(X + 1, Y));
            }
            
            if (Y - 1 >= 0 && ((board[X + 1][Y - 1] == null) || (board[X + 1][Y - 1] != null && !ComparePieceColors(board[X + 1][Y - 1])))) {
                possibleMoves.add(new BoardPoint(X + 1, Y - 1));
            }
            if (Y + 1 < y && ((board[X + 1][Y + 1] == null) || (board[X + 1][Y +1] != null && !ComparePieceColors(board[X + 1][Y + 1])))) {
                possibleMoves.add(new BoardPoint(X + 1, Y + 1));
            }
        }
        if (Y + 1 < y && ((board[X][Y + 1] == null) || (board[X][Y + 1] != null && !ComparePieceColors(board[X][Y + 1])))) {
            possibleMoves.add(new BoardPoint(X, Y + 1));
        }
        if (Y - 1 >= 0 && ((board[X][Y - 1] == null) || (board[X][Y - 1] != null && !ComparePieceColors(board[X][Y - 1])))) {
            possibleMoves.add(new BoardPoint(X, Y - 1));
        }
        return possibleMoves;
    }

    public ArrayList<BoardPoint> KingNonAccessible(BoardPoint dimensions, Piece[][] board) {
        ArrayList<BoardPoint> EnemyMoves = new ArrayList<>();
        for (int i = 0; i < dimensions.getX(); i++) {
            for (int j = 0; j < dimensions.getY(); j++) {
                if (board[i][j] != null && !board[i][j].ComparePieceColors(this)) {
                    ArrayList<BoardPoint> temp = board[i][j].getThreatArea(dimensions, board);
                    if (!temp.isEmpty()) {
                        EnemyMoves.addAll(temp);
                    }
                }
            }
        }
        return EnemyMoves;
    }
    
    @Override
    public ArrayList<BoardPoint> getThreatArea(BoardPoint dimensions, Piece[][] board) {
        ArrayList<BoardPoint> possibleMoves = new ArrayList<>();
        int x = dimensions.getX();
        int y = dimensions.getY();
        int X = position.getX();
        int Y = position.getY();
        if (X - 1 >= 0) {
            if ((board[X - 1][Y] == null) || (board[X - 1][Y] != null)) {
                possibleMoves.add(new BoardPoint(X - 1, Y));
            }
            
            if (Y - 1 >= 0 && ((board[X - 1][Y - 1] == null) || (board[X - 1][Y - 1] != null))) {
                possibleMoves.add(new BoardPoint(X - 1, Y - 1));
            }
            if (Y + 1 < y && ((board[X - 1][Y + 1] == null) || (board[X - 1][Y + 1] != null))) {
                possibleMoves.add(new BoardPoint(X - 1, Y + 1));
            }
        }
        if (X + 1 < x) {
            if ((board[X + 1][Y] == null) || (board[X + 1][Y] != null)) {
                possibleMoves.add(new BoardPoint(X + 1, Y));
            }
            
            if (Y - 1 >= 0 && ((board[X + 1][Y - 1] == null) || (board[X + 1][Y - 1] != null))) {
                possibleMoves.add(new BoardPoint(X + 1, Y - 1));
            }
            if (Y + 1 < y && ((board[X + 1][Y + 1] == null) || (board[X + 1][Y +1] != null))) {
                possibleMoves.add(new BoardPoint(X + 1, Y + 1));
            }
        }
        if (Y + 1 < y && ((board[X][Y + 1] == null) || (board[X][Y + 1] != null))) {
            possibleMoves.add(new BoardPoint(X, Y + 1));
        }
        if (Y - 1 >= 0 && ((board[X][Y - 1] == null) || (board[X][Y - 1] != null))) {
            possibleMoves.add(new BoardPoint(X, Y - 1));
        }
        return possibleMoves;
    }
    @Override
    public Piece GetCopy() {
        return (new King(isWhite,position));
    }
}
