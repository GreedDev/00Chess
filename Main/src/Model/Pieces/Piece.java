package Model.Pieces;

import Model.Board.BoardPoint;
import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author mvard
 * @author darthinvader
 */
public abstract class Piece {

    protected boolean isWhite;
    protected int turnMoved;
    protected BoardPoint position = new BoardPoint();

    public Piece(boolean isWhite, BoardPoint position) {
        this.isWhite = isWhite;
        this.position = position;
        this.turnMoved = 0;
    }

    public BoardPoint getPosition() {
        return position;
    }

    public boolean getPieceColor() {
        return isWhite;
    }

    public abstract ArrayList<BoardPoint> getPossibleMoves(BoardPoint dimensions, Piece[][] board);
    public abstract ArrayList<BoardPoint> getThreatArea(BoardPoint dimensions,Piece[][] board);
    public boolean ComparePieceColors(Piece p) {
        return (p.getPieceColor() == this.getPieceColor());
    }
    public void setPosition(BoardPoint pos){
        position = pos;
    }
    public void setTurnMoved(int turnMoved){
        this.turnMoved = turnMoved;
    }
    public void Move(BoardPoint pos,int turnMoved){
        this.turnMoved = turnMoved;
        this.position = pos;
    }
    public abstract Piece GetCopy();
}
