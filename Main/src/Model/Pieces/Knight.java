package Model.Pieces;

import Model.Board.BoardPoint;
import java.util.ArrayList;

/**
 *
 * @author mvard
 * @author darthinvader
 */
public class Knight extends Piece {

    public Knight(boolean isWhite, BoardPoint position) {
        super(isWhite, position);
    }

    @Override
    public ArrayList<BoardPoint> getPossibleMoves(BoardPoint dimensions, Piece[][] board) {
        ArrayList<BoardPoint> PossibleMoves = new ArrayList<>();
        BoardPoint buffer;
        buffer = getUpperRightGamma(dimensions, board,false);
        if (buffer != null) {
            PossibleMoves.add(buffer);
        }
        buffer = getUpperLeftGamma(dimensions, board,false);
        if (buffer != null) {
            PossibleMoves.add(buffer);
        }
        buffer = getDownRightGamma(dimensions, board,false);
        if (buffer != null) {
            PossibleMoves.add(buffer);
        }
        buffer = getDownLeftGamma(dimensions, board,false);
        if (buffer != null) {
            PossibleMoves.add(buffer);
        }
        buffer = getRightUpGamma(dimensions, board,false);
        if (buffer != null) {
            PossibleMoves.add(buffer);
        }
        buffer = getRightDownGamma(dimensions, board,false);
        if (buffer != null) {
            PossibleMoves.add(buffer);
        }
        buffer = getLeftUpGamma(dimensions, board,false);
        if (buffer != null) {
            PossibleMoves.add(buffer);
        }
        buffer = getLeftDownGamma(dimensions, board,false);
        if (buffer != null) {
            PossibleMoves.add(buffer);
        }
        return PossibleMoves;
    }

    private BoardPoint getUpperRightGamma(BoardPoint dimensions, Piece[][] board,boolean Threat) {
        int x = position.getX() + 2;
        int y = position.getY() + 1;
        BoardPoint ret = new BoardPoint(x, y);
        if (x >= dimensions.getX() || y >= dimensions.getY()) {
            return null;
        }
        if (board[x][y] != null && board[x][y].ComparePieceColors(this) && !Threat) {
            return null;
        }
        return ret;
    }

    private BoardPoint getUpperLeftGamma(BoardPoint dimensions, Piece[][] board,boolean Threat) {
        int x = position.getX() + 2;
        int y = position.getY() - 1;
        BoardPoint ret = new BoardPoint(x, y);
        if (x >= dimensions.getX() || y < 0) {
            return null;
        }
        if (board[x][y] != null && board[x][y].ComparePieceColors(this) && !Threat) {
            return null;
        }
        return ret;
    }

    private BoardPoint getDownRightGamma(BoardPoint dimensions, Piece[][] board,boolean Threat) {
        int x = position.getX() - 2;
        int y = position.getY() + 1;
        BoardPoint ret = new BoardPoint(x, y);
        if (x < 0 || y >= dimensions.getY()) {
            return null;
        }
        if (board[x][y] != null && board[x][y].ComparePieceColors(this) && !Threat) {
            return null;
        }
        return ret;
    }

    private BoardPoint getDownLeftGamma(BoardPoint dimensions, Piece[][] board,boolean Threat) {
        int x = position.getX() - 2;
        int y = position.getY() - 1;
        BoardPoint ret = new BoardPoint(x, y);
        if (x < 0 || y < 0) {
            return null;
        }
        if (board[x][y] != null && board[x][y].ComparePieceColors(this) && !Threat) {
            return null;
        }
        return ret;
    }

    private BoardPoint getRightUpGamma(BoardPoint dimensions, Piece[][] board,boolean Threat) {
        int x = position.getX() + 1;
        int y = position.getY() + 2;
        BoardPoint ret = new BoardPoint(x, y);
        if (x >= dimensions.getX() || y >= dimensions.getY()) {
            return null;
        }
        if (board[x][y] != null && board[x][y].ComparePieceColors(this) && !Threat) {
            return null;
        }
        return ret;
    }

    private BoardPoint getRightDownGamma(BoardPoint dimensions, Piece[][] board,boolean Threat) {
        int x = position.getX() - 1;
        int y = position.getY() + 2;
        BoardPoint ret = new BoardPoint(x, y);
        if (x < 0 || y >= dimensions.getY()) {
            return null;
        }
        if (board[x][y] != null && board[x][y].ComparePieceColors(this) && !Threat) {
            return null;
        }
        return ret;
    }

    private BoardPoint getLeftUpGamma(BoardPoint dimensions, Piece[][] board,boolean Threat) {
        int x = position.getX() + 1;
        int y = position.getY() - 2;
        BoardPoint ret = new BoardPoint(x, y);
        if (x >= dimensions.getX() || y < 0) {
            return null;
        }
        if (board[x][y] != null && (board[x][y].ComparePieceColors(this) && !Threat)) {
            return null;
        }
        return ret;
    }

    private BoardPoint getLeftDownGamma(BoardPoint dimensions, Piece[][] board,boolean Threat) {
        int x = position.getX() - 1;
        int y = position.getY() - 2;
        BoardPoint ret = new BoardPoint(x, y);
        if (x < 0 || y < 0) {
            return null;
        }
        if (board[x][y] != null && (board[x][y].ComparePieceColors(this) && !Threat)) {
            return null;
        }
        return ret;
    }

    @Override
    public ArrayList<BoardPoint> getThreatArea(BoardPoint dimensions, Piece[][] board) {
        ArrayList<BoardPoint> threatArea = new ArrayList<>();
        BoardPoint buffer;
        buffer = getUpperRightGamma(dimensions, board,true);
        if (buffer != null) {
            threatArea.add(buffer);
        }
        buffer = getUpperLeftGamma(dimensions, board,true);
        if (buffer != null) {
            threatArea.add(buffer);
        }
        buffer = getDownRightGamma(dimensions, board,true);
        if (buffer != null) {
            threatArea.add(buffer);
        }
        buffer = getDownLeftGamma(dimensions, board,true);
        if (buffer != null) {
            threatArea.add(buffer);
        }
        buffer = getRightUpGamma(dimensions, board,true);
        if (buffer != null) {
            threatArea.add(buffer);
        }
        buffer = getRightDownGamma(dimensions, board,true);
        if (buffer != null) {
            threatArea.add(buffer);
        }
        buffer = getLeftUpGamma(dimensions, board,true);
        if (buffer != null) {
            threatArea.add(buffer);
        }
        buffer = getLeftDownGamma(dimensions, board,true);
        if (buffer != null) {
            threatArea.add(buffer);
        }
        return threatArea;
    }
    @Override
    public Piece GetCopy() {
        return (new Knight(isWhite,position));
    }
}
