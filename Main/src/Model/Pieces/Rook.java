package Model.Pieces;

import Model.Board.BoardPoint;
import java.util.ArrayList;

/**
 *
 * @author mvard
 * @author darthinvader
 */
public class Rook extends Piece {

    public Rook(boolean isWhite, BoardPoint position) {
        super(isWhite, position);
    }

    @Override
    public ArrayList<BoardPoint> getPossibleMoves(BoardPoint dimensions, Piece[][] board) {
        ArrayList<BoardPoint> possibleMoves = new ArrayList<>();
        ArrayList<BoardPoint> dummy = RookLeft(dimensions,board,false);
        if(!dummy.isEmpty()){
            possibleMoves.addAll(dummy);
        }
        dummy = RookUp(dimensions,board,false);
        if(!dummy.isEmpty()){
            possibleMoves.addAll(dummy);
        }
        dummy = RookRight(dimensions,board,false);
        if(!dummy.isEmpty()){
            possibleMoves.addAll(dummy);
        }
        dummy = RookDown(dimensions,board,false);
        if(!dummy.isEmpty()){
            possibleMoves.addAll(dummy);
        }
        return possibleMoves;
    }
    
    private ArrayList<BoardPoint> RookLeft(BoardPoint dimensions, Piece[][] board,boolean Threat) {
        ArrayList<BoardPoint> Rook = new ArrayList<>();
        int x = position.getX();
        int y = position.getY();
        while (true) {
            x++;
            if (x >= dimensions.getX()) {
                break;
            }
            if (board[x][y] != null) {
                if (board[x][y].ComparePieceColors(this)) {
                    if(Threat){
                        Rook.add(new BoardPoint(x,y));
                    }
                    break; 
                } else if(!(board[x][y] instanceof King) || !Threat){
                    Rook.add(new BoardPoint(x,y));
                    break;
                }
            }
            Rook.add(new BoardPoint(x, y));
        }
        return Rook;
    }
    
    private ArrayList<BoardPoint> RookUp(BoardPoint dimensions, Piece[][] board,boolean Threat) {
        ArrayList<BoardPoint> Rook = new ArrayList<>();
        int x = position.getX();
        int y = position.getY();
        while (true) {
            y++;
            if (y >= dimensions.getY()) {
                break;
            }
            if (board[x][y] != null) {
                if (board[x][y].ComparePieceColors(this)) {
                    if(Threat){
                        Rook.add(new BoardPoint(x,y));
                    }
                    break;
                } else if(!(board[x][y] instanceof King) || !Threat){
                    Rook.add(new BoardPoint(x,y));
                    break;
                }
            }
            Rook.add(new BoardPoint(x, y));
        }
        return Rook;
    }
    
    private ArrayList<BoardPoint> RookRight(BoardPoint dimensions, Piece[][] board,boolean Threat) {
        ArrayList<BoardPoint> Rook = new ArrayList<>();
        int x = position.getX();
        int y = position.getY();
        while (true) {
            x--;
            if (x<0) {
                break;
            }
            if (board[x][y] != null) {
                if (board[x][y].ComparePieceColors(this)) {
                    if(Threat){
                        Rook.add(new BoardPoint(x,y));
                    }
                    break;
                } else if(!(board[x][y] instanceof King) || !Threat){
                    Rook.add(new BoardPoint(x,y));
                    break;
                }
            }
            Rook.add(new BoardPoint(x, y));
        }
        return Rook;
    }
    private ArrayList<BoardPoint> RookDown(BoardPoint dimensions, Piece[][] board,boolean Threat) {
        ArrayList<BoardPoint> Rook = new ArrayList<>();
        int x = position.getX();
        int y = position.getY();
        while (true) {
            y--;
            if (y<0) {
                break;
            }
            if (board[x][y] != null) {
                if (board[x][y].ComparePieceColors(this)) {
                    if(Threat){
                        Rook.add(new BoardPoint(x,y));
                    }
                    break;
                } else if(!(board[x][y] instanceof King) || !Threat){
                    Rook.add(new BoardPoint(x,y));
                    break;
                }
            }
            Rook.add(new BoardPoint(x, y));
        }
        return Rook;
    }
    
    @Override
    public ArrayList<BoardPoint> getThreatArea(BoardPoint dimensions, Piece[][] board) {
        ArrayList<BoardPoint> possibleMoves = new ArrayList<>();
        ArrayList<BoardPoint> dummy = RookLeft(dimensions,board,true);
        if(!dummy.isEmpty()){
            possibleMoves.addAll(dummy);
        }
        dummy = RookUp(dimensions,board,true);
        if(!dummy.isEmpty()){
            possibleMoves.addAll(dummy);
        }
        dummy = RookRight(dimensions,board,true);
        if(!dummy.isEmpty()){
            possibleMoves.addAll(dummy);
        }
        dummy = RookDown(dimensions,board,true);
        if(!dummy.isEmpty()){
            possibleMoves.addAll(dummy);
        }
        return possibleMoves;
    }
    @Override
    public Piece GetCopy() {
        return (new Rook(isWhite,position));
    }
}
