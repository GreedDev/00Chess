package Model.Pieces;

import Model.Board.BoardPoint;
import java.util.ArrayList;

/**
 *
 * @author mvard
 * @author darthinvader
 */
public class Pawn extends Piece {

    public Pawn(boolean isWhite, BoardPoint position) {
        super(isWhite, position);
    }

    @Override
    public ArrayList<BoardPoint> getPossibleMoves(BoardPoint dimensions, Piece[][] board) {
        int x = dimensions.getX();
        int y = dimensions.getY();
        int X = position.getX();
        int Y = position.getY();
        ArrayList<BoardPoint> possibleMoves = new ArrayList<>();
        if (isWhite == true) {
            if (X + 2 < x && turnMoved == 0) {
                System.out.println("moved");
                if (board[X + 2][Y] == null && board[X+1][Y] == null) {
                    System.out.println("why not here?");
                    possibleMoves.add(new BoardPoint(X + 2, Y));
                }
            }
            if (X + 1 < x) {
                if (board[X + 1][Y] == null) {
                    possibleMoves.add(new BoardPoint(X + 1, Y));
                }
                if (Y + 1 < y && board[X + 1][Y + 1] != null && !ComparePieceColors(board[X + 1][Y + 1])) {
                    possibleMoves.add(new BoardPoint(X + 1, Y + 1));
                }
                if (Y - 1 >= 0 && board[X + 1][Y - 1] != null && !ComparePieceColors(board[X + 1][Y - 1])) {
                    possibleMoves.add(new BoardPoint(X + 1, Y - 1));
                }
            }
        } else {
            if (X - 2 >= 0 && turnMoved == 0) {
                if (board[X - 2][Y] == null && board[X-1][Y]==null) {
                    possibleMoves.add(new BoardPoint(X - 2, Y));
                }
            }
            if (X - 1 >= 0) {
                if (board[X - 1][Y] == null) {
                    possibleMoves.add(new BoardPoint(X - 1, Y));
                }
                if (Y + 1 < y && board[X - 1][Y + 1] != null && !ComparePieceColors(board[X - 1][Y + 1])) {
                    possibleMoves.add(new BoardPoint(X - 1, Y + 1));
                }
                if (Y - 1 >= 0 && board[X - 1][Y - 1] != null && !ComparePieceColors(board[X - 1][Y - 1])) {
                    possibleMoves.add(new BoardPoint(X - 1, Y - 1));
                }
            }
        }
        return possibleMoves;
    }

    @Override
    public ArrayList<BoardPoint> getThreatArea(BoardPoint dimensions, Piece[][] board) {
        int x = dimensions.getX();
        int y = dimensions.getY();
        int X = position.getX();
        int Y = position.getY();
        ArrayList<BoardPoint> threatArea = new ArrayList<>();
        if (isWhite == true) {
            if (X + 1 < x) {
                if (Y + 1 < y) {
                    threatArea.add(new BoardPoint(X + 1, Y + 1));
                }
                if (Y - 1 >= 0) {
                    threatArea.add(new BoardPoint(X + 1, Y - 1));
                }
            }
        } else {
            if (Y + 1 < y) {
                threatArea.add(new BoardPoint(X - 1, Y + 1));
            }
            if (Y - 1 >= 0) {
                threatArea.add(new BoardPoint(X - 1, Y - 1));
            }
        }        
        return threatArea;
    }
    @Override
    public Piece GetCopy() {
        return (new Pawn(isWhite,position));
    }
}
