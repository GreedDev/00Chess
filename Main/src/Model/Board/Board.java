package Model.Board;

import Model.Pieces.*;
import java.util.ArrayList;

/**
 *
 * @author mvard
 * @author darthinvader
 */
public class Board {

    private Piece[][] board;
    private int Turn;//Number of Total Turns passed
    private boolean PlayerTurn;//0 for black 1 for white
    private BoardPoint Dimensions;
    private BoardPoint LastTurnPawnDoubleMove;

    /**
     * This constructor takes all the possible data to create itself It's main
     * use is for loading saved games
     *
     * @param board the board state with all the pieces
     * @param Turn the turn measured by the ammount of times a pawn moves
     * @param PlayerTurn Which player's turn is it
     * @param Dimensions The dimensions of the array
     */
    public Board(Piece[][] board, int Turn, boolean PlayerTurn, BoardPoint Dimensions,BoardPoint LastTurnPawnDoubleMove) {
        this.board = board;
        this.Turn = Turn;
        this.PlayerTurn = PlayerTurn;
        this.Dimensions = Dimensions;
    }

    public Board(Piece[][] board, int Turn, boolean PlayerTurn, BoardPoint Dimensions) {
        this.board = board;
        this.Turn = Turn;
        this.PlayerTurn = PlayerTurn;
        this.Dimensions = Dimensions;
        this.LastTurnPawnDoubleMove = LastTurnPawnDoubleMove;
    }

    public Board() {
        board = new Piece[8][8];
        Dimensions = new BoardPoint(8, 8);
        Turn = 1;
        PlayerTurn = true;
        LastTurnPawnDoubleMove = null;
        board[0][0] = new Rook(true, new BoardPoint(0, 0));
        board[0][1] = new Knight(true, new BoardPoint(0, 1));
        board[0][2] = new Bishop(true, new BoardPoint(0, 2));
        board[0][3] = new Queen(true, new BoardPoint(0, 3));
        board[0][4] = new King(true, new BoardPoint(0, 4));
        board[0][5] = new Bishop(true, new BoardPoint(0, 5));
        board[0][6] = new Knight(true, new BoardPoint(0, 6));
        board[0][7] = new Rook(true, new BoardPoint(0, 7));
        for (int i = 0; i < 8; i++) {
            board[1][i] = new Pawn(true, new BoardPoint(1, i));
        }
        board[0][0] = new Rook(true, new BoardPoint(0, 0));
        board[7][0] = new Rook(false, new BoardPoint(7, 0));
        board[7][1] = new Knight(false, new BoardPoint(7, 1));
        board[7][2] = new Bishop(false, new BoardPoint(7, 2));
        board[7][3] = new Queen(false, new BoardPoint(7, 3));
        board[7][4] = new King(false, new BoardPoint(7, 4));
        board[7][5] = new Bishop(false, new BoardPoint(7, 5));
        board[7][6] = new Knight(false, new BoardPoint(7, 6));
        board[7][7] = new Rook(false, new BoardPoint(7, 7));
        for (int i = 0; i < 8; i++) {
            board[6][i] = new Pawn(false, new BoardPoint(6, i));
        }
    }

    /**
     * Returns the state of the Board
     *
     * @return board
     */
    public Piece[][] getBoardState() {
        return board;
    }
    
    public int MovePiece(BoardPoint curPos, BoardPoint newPos) {
        int curx = curPos.getX();
        int cury = curPos.getY();
        int newx = newPos.getX();
        int newy = newPos.getY();
        int returner = 0;
        if (curx < 0 || curx >= Dimensions.getX() || cury < 0 && cury >= Dimensions.getY()) {
            return -1;
        }
        Piece currPiece = board[curx][cury];
        if (currPiece == null) {
            return -2;
        }
        ArrayList<BoardPoint> PossibleMoves = getPiecePossibleMoves(curPos);
        boolean flag = false;
        for (BoardPoint p : PossibleMoves) {
            if (p.equals(newPos)) {
                flag = true;
                break;
            }
        }
        if (flag == false) {
            return -3;
        }
        
        if(this.isKingThreatened(curPos, newPos)){
            return -4;
        }
        LastTurnPawnDoubleMove =null;
        if(currPiece instanceof Pawn){
            if(Math.abs(newx - curx) == 2){
                LastTurnPawnDoubleMove = newPos;
            }
            else if(currPiece.getPossibleMoves(Dimensions, board).size()!= PossibleMoves.size()){
                System.out.println("i'm here");
                if(currPiece.getPieceColor()){
                    board[newx-1][newy] = null;
                }
                else{
                    board[newx+1][newy] =null;
                }
            }
            else{
                if(newx == 7 || newx==0){
                    returner = 1;
                }
            }
        }
        board[curx][cury].Move(newPos,Turn);
        board[newx][newy] = board[curx][cury];
        board[curx][cury] = null;
        PlayerTurn = !PlayerTurn;
        Turn++;
        
        return returner;
    }

    public void PrintBoardState() {
        int x = Dimensions.getX();
        int y = Dimensions.getY();
        for (int i = 0; i < x; i++) {
            System.out.println("");
            for (int j = 0; j < y; j++) {
                Piece k = board[i][j];
                if (k == null) {
                    System.out.print("|  ");
                } else if (k instanceof King) {
                    System.out.print("| K");
                }
                if (k instanceof Pawn) {
                    System.out.print("| P");
                }
                if (k instanceof Rook) {
                    System.out.print("| R");
                }
                if (k instanceof Bishop) {
                    System.out.print("| B");
                }
                if (k instanceof Queen) {
                    System.out.print("| Q");
                }
                if (k instanceof Knight) {
                    System.out.print("| k");
                }
            }
        }
    }

    public ArrayList<BoardPoint> getPiecePossibleMoves(BoardPoint position) {
        if (!position.boardCheck(Dimensions)) {
            return null;
        }
        int X = position.getX();
        int Y = position.getY();
        Piece curr = board[X][Y];
        if (curr == null) {
            return null;
        }
        ArrayList<BoardPoint> PossibleMoves = curr.getPossibleMoves(Dimensions, board);
        if (curr instanceof Pawn && LastTurnPawnDoubleMove != null) {
            BoardPoint p1 = new BoardPoint(X, Y + 1);
            BoardPoint p2 = new BoardPoint(X, Y - 1);
            if (p1.equals(LastTurnPawnDoubleMove) || p2.equals(LastTurnPawnDoubleMove)) {
                if (curr.getPieceColor()) {
                    PossibleMoves.add(new BoardPoint(LastTurnPawnDoubleMove.getX() + 1, LastTurnPawnDoubleMove.getY()));
                } else {
                    PossibleMoves.add(new BoardPoint(LastTurnPawnDoubleMove.getX() - 1, LastTurnPawnDoubleMove.getY()));
                }
            }
        }
        return PossibleMoves;
    }

    public Piece getPiece(BoardPoint position) {
        if (position.getX() >= Dimensions.getX() || position.getX() < 0 || position.getY() >= Dimensions.getY() || position.getY() < 0) {
            return null;
        }
        return board[position.getX()][position.getY()];
    }

    public int getTurn() {
        return this.Turn;
    }

    public boolean getPlayerTurn() {
        return this.PlayerTurn;
    }

    private ArrayList<BoardPoint> getThreatOfColor(boolean isWhite) {

        ArrayList<BoardPoint> threatArea = new ArrayList<>();
        ArrayList<BoardPoint> buffer;
        for (int i = 0; i < Dimensions.getX(); i++) {
            for (int j = 0; j < Dimensions.getY(); j++) {
                if (board[i][j] != null && board[i][j].getPieceColor() == isWhite) {
                    buffer = board[i][j].getThreatArea(Dimensions, board);
                    if (!buffer.isEmpty()) {
                        threatArea.addAll(buffer);
                    }
                }
            }
        }
        return threatArea;
    }

    private Piece findKingColor(boolean PlayerTurn) {
        for(int i=0;i<8;i++){
            for(int j=0;j<8;j++){
                if(board[i][j] instanceof King && board[i][j].getPieceColor()==PlayerTurn){
                    return board[i][j];
                }
            }
        }
        return null;
    }
    private boolean isKingThreatened(BoardPoint curPos,BoardPoint newPos){
        int curx = curPos.getX();
        int cury = curPos.getY();
        Piece newPosPiece = board[curx][cury].GetCopy();
        newPosPiece.Move(newPos,Turn);
        Piece[][] tempb = new Piece[8][8];
        BoardPoint x;
        
        for(int i=0;i<8;i++){
            for(int j=0;j<8;j++){
                x = new BoardPoint(i,j);
                if(x.equals(curPos)){
                    tempb[i][j] = null;
                }
                else if(x.equals(newPos)){
                    tempb[i][j] = newPosPiece;
                    System.out.println("here" + newPos.getX() +"" + newPos.getY());
                }
                else{
                    tempb[i][j] = board[i][j];
                }
            }
        }
        Board temp = new Board(tempb,Turn,PlayerTurn,Dimensions,LastTurnPawnDoubleMove);
        Piece currKing = temp.findKingColor(PlayerTurn);
        ArrayList<BoardPoint> enemyThreatArea = temp.getThreatOfColor(!PlayerTurn);
        for(BoardPoint p:enemyThreatArea){
            if(p.equals(currKing.getPosition())){
                return true;
            }
        }
        return false;
    }
    public void promotePawn(int x,BoardPoint pos){
        if(x == 1){
            board[pos.getX()][pos.getY()]  = new Rook(!PlayerTurn, pos);
        }
        if(x == 2){
            board[pos.getX()][pos.getY()]  = new Knight(!PlayerTurn, pos);
        }
        if(x == 3){
            board[pos.getX()][pos.getY()]  = new Bishop(!PlayerTurn, pos);
        }
        if(x == 4){
            board[pos.getX()][pos.getY()]  = new Queen(!PlayerTurn, pos);
        }
        
    }
    
}
