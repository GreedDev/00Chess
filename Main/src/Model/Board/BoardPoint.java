/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model.Board;

/**
 *
 * @author darthinvader
 */
public class BoardPoint {

    private int x;
    private int y;

    public BoardPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public BoardPoint() {
        this.x = 0;
        this.y = 0;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
    public boolean equals(BoardPoint b) {
        return (b.getX() == x && b.getY() == y);
    }
    public boolean boardCheck(BoardPoint dimensions){
        return(x>=0 && x<dimensions.getX() && y>=0 && y<dimensions.getY());
    }
    public boolean boardCheck(BoardPoint dimensions,int Xshift,int Yshift){
        int X = x +Xshift;
        int Y = y+Yshift;
        return(X>=0 && X<dimensions.getX() && Y>=0 && Y<dimensions.getY());
    }
}
